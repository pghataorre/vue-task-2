import {shallowMount} from '@vue/test-utils'
import navigation from '../../src/components/navigation/navigation.vue';


describe('navigation.vue', () => {

  it('is a vue instance', () => {
    const wrapper = shallowMount(navigation);
    expect(wrapper.isVueInstance()).toBeTruthy();
  });
});