export default {
  computed: {
    basketItemsCount() {
      let basketItemsCount =
        this.$store.getters["phoneListModule/basketItems"].length !== -1
          ? this.$store.getters["phoneListModule/basketItems"].length
          : 0;

      return basketItemsCount;
    },
    totalCalculation() {
      if (this.basketItemsCount > 0) {
        const basketItems = this.$store.getters["phoneListModule/basketItems"];
        const totalsArr = basketItems.map(item => {
          return item.price;
        });
        const total = totalsArr.reduce(function(a, b) {
          return a + b;
        });

        return total;
      }

      return 0;
    }
  }
};