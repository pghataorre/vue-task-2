export default {
  namespaced: true,
  state: {
    taskList: []
  },
  getters: {
    skillsList(state) {
      return state.skillsList;
    }
  },
  mutations: {
    completeSkill(state, id) {
      const skillItemObj = state.skillsList[id];

      skillItemObj.completed = true;
      skillItemObj.skillVal = state.skillsList[id].skillVal += 1;
      state.skillsList[id] = skillItemObj;
    },
    redoSkill(state, id) {
      const skillItemObj = state.skillsList[id];

      if (state.skillsList[id].skillVal > 0) {
        skillItemObj.completed = false;
        skillItemObj.skillVal = state.skillsList[id].skillVal -= 1;
        state.skillsList[id] = skillItemObj;
      }
    }
  },
  actions: {
    postTask({ state }, payload) {
      return fetch("https://jsonplaceholder.typicode.com/posts", {
        method: "POST",
        body: JSON.stringify({
          title: payload.title,
          body: payload.taskDetail,
          userId: 1
        }),
        headers: {
          "Content-type": "application/json; charset=UTF-8"
        }
      })
        .then(response => {
          return response.json();
        })
        .then(response => {
          state.taskList.unshift(response);
          return response;
        })
        .catch(error => {
          return [{ error }];
        });
    },
    getTasksFetch({ state }) {
      return fetch("https://jsonplaceholder.typicode.com/posts", {
        method: "GET",
        mode: "cors",
        headers: {
          "Access-Control-Allow-Origin": "*"
        }
      })
        .then(response => {
          return response.json();
        })
        .then(response => {
          state.taskList = response;
          return response;
        })
        .catch(error => {
          return [{ error }];
        });
    }
  }
};