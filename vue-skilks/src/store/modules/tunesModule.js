export default {
  namespaced: true,
  state: {
    tunesList: [],
    tunesCount: 0,
    videoPreviewUrl: null
  },
  getters: {
    getTuneResults(state) {
      return state.tunesList;
    },
    getTunesCount(state) {
      return state.tunesCount;
    },
    getTaskList(state) {
      return state.taskList;
    },
    videoPreviewUrl(state) {
      return state.videoPreviewUrl;
    }
  },
  mutations: {
    setVideoPreviewUrl(state, previewUrl) {
      state.videoPreviewUrl = previewUrl;
    }
  },
  actions: {
    searchTunesApi(storeObj, tuneSearchObj) {
      let { dispatch, state } = storeObj;

      if (tuneSearchObj.tuneSearchVal) {
        let { tuneSearchVal, selectAmount, previewType } = tuneSearchObj;

        let selectAmountVal = selectAmount ? selectAmount : 10;
        let urlPath = `https://itunes.apple.com/search?term=${tuneSearchVal}&limit=${selectAmountVal}&entity=${previewType}`;

        dispatch("fetchApi", {
          urlPath: urlPath,
          state
        });
      }
    },
    fetchApi() {
      let state = arguments[1].state;
      let urlPath = arguments[1].urlPath;

      fetch(urlPath, {
        mode: "cors",
        headers: {
          "Access-Control-Allow-Origin": "*"
        }
      })
        .then(response => {
          return response.json();
        })
        .then(response => {
          if (response) {
            state.tunesList = response.results;
            state.tunesCount = response.resultCount;
          }
        })
        .catch(error => {
          state.tunesList.length = 0;
          state.tunesCount = 0;
          console.log("ERROR OCCURRED", error);
          return error;
        });
    }
  }
};