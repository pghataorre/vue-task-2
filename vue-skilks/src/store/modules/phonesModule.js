export default {
  namespaced: true,
  state: {
    phoneList: [
      {
        id: 0,
        name: "Samsung Galaxy S10+",
        price: 35,
        description:
          "Samsung Galaxy S10+ - Price includes device(s) and plan.No upfront costs.",
        colors: [
          "Prism Black",
          "Prism White",
          "Prism Green",
          "Ceramic Black",
          "Ceramic White"
        ],
        inStock: 10,
        phoneReviews: [
          {
            reviewerName: "Permy Ghataorre 1",
            reviewerEmail: "permy1.ghataorre@gmail.com",
            reviewComment: `Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.It has survived not only five`
          },
          {
            reviewerName: "Permy Ghataorre 2",
            reviewerEmail: "permy2.ghataorre@gmail.com",
            reviewComment: `Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ips`
          },
          {
            reviewerName: "Permy Ghataorre 3",
            reviewerEmail: "permy3.ghataorre@gmail.com",
            reviewComment: `Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ips of the printing and typesetting industry. Lorem Ips`
          }
        ]
      },
      {
        id: 1,
        name: "Apple iPhone 7",
        price: 19,
        description: "Apple iPhone 7 - Description test of an Apple",
        colors: ["Prism", "Oil White", "Green", "Ceramic", "Ceramic White"],
        inStock: 10,
        phoneReviews: [
          {
            reviewerName: "Permy Ghataorre",
            reviewerEmail: "permy.ghataorre@gmail.com",
            reviewComment:
              "this is a test review for this phone, this is a test review for this phone"
          }
        ]
      },
      {
        id: 2,
        name: "Sony Xperia 10 with Sony Bluetooth Speaker",
        price: 24,
        description: "Price includes device(s) and plan.No upfront costs.",
        colors: ["Red", "Green", "Blue", "Orange", "Purple"],
        phoneReviews: [
          {
            reviewerName: "Permy Ghataorre",
            reviewerEmail: "permy.ghataorre@gmail.com",
            reviewComment:
              "this is a test review for this phone, this is a test review for this phone"
          }
        ]
      },
      {
        id: 3,
        name: "HuaweiP20 Pro",
        price: 45,
        description:
          "HuaweiP20 Pro - Price includes device(s) and plan.No upfront costs.",
        colors: ["Prism Black"],
        phoneReviews: [
          {
            reviewerName: "Permy Ghataorre",
            reviewerEmail: "permy.ghataorre@gmail.com",
            reviewComment:
              "this is a test review for this phone, this is a test review for this phone"
          }
        ]
      },
      {
        id: 4,
        name: "One PLUS 2",
        price: 19,
        description: "One PLUS 6 - Description test of an Apple",
        colors: ["White"],
        phoneReviews: [
          {
            reviewerName: "Permy Ghataorre",
            reviewerEmail: "permy.ghataorre@gmail.com",
            reviewComment:
              "this is a test review for this phone, this is a test review for this phone"
          }
        ]
      },
      {
        id: 5,
        name: "Huawei Y7 (2018)",
        price: 67,
        description:
          "Huawei Y7 (2018) - Price includes device(s) and plan.No upfront costs.",
        colors: ["Orange", "Purple"],
        phoneReviews: []
      }
    ],
    basketItems: [],
    showAddReviewOverlay: false,
    showFullDetailBasket: false,
    reviewId: null
  },
  getters: {
    phoneList(state) {
      return state.phoneList;
    },
    basketItems(state) {
      return state.basketItems;
    },
    isBasketEmpty(state) {
      return state.basketItems.length > 0;
    },
    showHiAddReviewOverlay(state) {
      return state.showAddReviewOverlay;
    },
    getReviewId(state) {
      return state.reviewId;
    },
    toggleFullDetailBasket(state) {
      return state.showFullDetailBasket;
    }
  },
  mutations: {
    openCloseFullDetailBasket(state) {
      state.showFullDetailBasket = !state.showFullDetailBasket;
    },
    openCloseOverylay(state) {
      state.showAddReviewOverlay = !state.showAddReviewOverlay;
    },
    removeBasketItem(state, id) {
      state.basketItems.splice(parseInt(id), 1);

      if (state.basketItems.length === 0) {
        state.showFullDetailBasket = false;
      }
    },
    setReviewId(state, id) {
      state.reviewId = id;
    },
    addToBasket(state, id) {
      const currentItem = state.phoneList[id];
      state.basketItems.push(currentItem);
    }
  },
  actions: {
    addReview({state}, reviewObj) {
      // Add as an action
      let id = state.reviewId
      state.phoneList[id].phoneReviews.push(reviewObj);
    }
  }
};