import Vue from 'vue';
import Vuex from 'vuex';
import phonesModule from './modules/phonesModule.js';
import tunesModule from './modules/tunesModule.js';
import tasksModule from './modules/tasksModule.js';

Vue.use(Vuex);

export const store = new Vuex.Store({
  modules: {
    phoneListModule: phonesModule,
    tunesModule,
    tasksModule
  },
  state: {
    reviewId: null
  }
});