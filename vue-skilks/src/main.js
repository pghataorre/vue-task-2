import Vue from 'vue';
import App from './App.vue';
import {store} from './store/store.js';
import router from './route/index.js';
import './firebaseConfig.js'

new Vue({
  render: h => h(App),
  store,
  router
}).$mount('#app')
