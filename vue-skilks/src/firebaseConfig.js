import firebase from "firebase";
import "firebase/firestore";

const config = {
  apiKey: "AIzaSyB62gQrEr1_wPuLqF4Bz0xetbBJJi2vWQA",
  authDomain: "phoneshopv2-f60e2.firebaseapp.com",
  databaseURL: "https://phoneshopv2-f60e2.firebaseio.com",
  projectId: "phoneshopv2-f60e2",
  storageBucket: "phoneshopv2-f60e2.appspot.com",
  messagingSenderId: "1007142454630",
  appId: "1:1007142454630:web:5ac569f35bb3541d"
};

firebase.initializeApp(config);

export const db = firebase.firestore();

//db.settings({timestampsInSnapshots: true});
