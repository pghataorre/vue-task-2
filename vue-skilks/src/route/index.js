import Vue from 'vue';
import Router from 'vue-router';
import phoneShop from '../phoneshop/phoneshop.vue';
import tasks from "../tasks/tasks.vue";
import myTunes from '../mytunes/mytunes'
import rootPage from '../rootpage/rootpage.vue';

const routes = [
  {
    path: "/",
    name: "DEFAULT",
    component: rootPage
  },
  {
    path: "/phoneshop",
    name: "PHONES SHOP",
    component: phoneShop
  },
  {
    path: "/tasks",
    name: "TASKS",
    component: tasks
  },
  {
    path: "/myTunes",
    name: "MY TUNES",
    component: myTunes
  }
];

Vue.use(Router);

export default new Router({
  routes
});